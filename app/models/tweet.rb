class Tweet < ActiveRecord::Base
  attr_accessible :entry_id, :message, :author
  belongs_to :entry
  after_create :send_tweet

  def send_tweet
    user = User.find_by_name self.author
    entry = self.entry

    File.join(Rails.root, 'public', 'images', entry.image.url)

    Twitter.configure do |config|
      config.consumer_key = APP_CONFIG['tw_app_id']
      config.consumer_secret = APP_CONFIG['tw_app_secret']
      # config.oauth_token = current_user.token
      # config.oauth_token_secret = current_user.secret
      config.oauth_token = user.token
      config.oauth_token_secret = user.secret
    end

    Twitter.update_with_media(self.message, File.new(File.join(Rails.root, 'public', entry.image.url)))
  end

end
