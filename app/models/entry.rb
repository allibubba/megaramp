class Entry < ActiveRecord::Base

  require 'carrierwave/orm/activerecord'
  mount_uploader :image, PhotoUploader

  attr_accessible :status, :image

  # has_many :tweets
  #accepts_nested_attributes_for :tweet

  after_create :demo

  def demo
    pp "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
      pp "creating #{self}"
    pp "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  end
end
