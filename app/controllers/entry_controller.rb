class EntryController < ApplicationController
  def index
    @entries = Entry.all
  end

  def tweet
    if current_user

      @entry = Entry.find params[:id]
      @message = params[:entry][:tweets][:message]
      @entry.tweets.create({
        author:current_user.name,
        message:@message
        })
      redirect_to signout_path
    else
      render :json => {:status=>"fail",message:"User not found"}.to_json
    end

  end
end