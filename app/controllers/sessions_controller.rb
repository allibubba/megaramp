class SessionsController < ActionController::Base
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    redirect_to root_url, notice: "Signed in!"
  end

  def destroy
    session[:user_id] = nil

    flash[:warn] = "Signed out!"
    redirect_to root_url
  end
end