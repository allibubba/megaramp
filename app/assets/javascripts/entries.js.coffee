# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

initTweetForm = () ->
  forms = document.getElementsByTagName("form")

  for form in forms
    submit =  form.getElementsByClassName('submitter')
    $(submit).bind 'click', (event) ->
      event.target.parentNode.submit()

$(initTweetForm)

initIsNotice = () ->

  notice = document.getElementsByClassName("alert")
  close = document.getElementsByClassName("close")
  setTimeout (-> $(notice).fadeOut('slow')), 1000


$(initIsNotice)