class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :status
      t.string :image
      t.timestamps
    end
  end
end
