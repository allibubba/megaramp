Megaramp::Application.routes.draw do
  root :to => 'Entry#index'
  match 'auth/:provider/callback', to: 'sessions#create'
  match 'auth/failure', to: redirect('/')
  match 'signout', to: 'sessions#destroy', as: 'signout'
  match 'tweet/:entry_id', to: 'tweet#create', as: 'tweet'
  match 'entry/:id', to: 'Entry#tweet', as: 'entry'
end
