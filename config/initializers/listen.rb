require 'listen'
require 'pp'

pub_images = Rails.root.join('public/images')
listener = Listen.to pub_images, force_polling: true  do |modified, added, removed|
  pp '---------------------------------------------------------------------------'
    puts modified
    puts added
    puts removed
  pp '---------------------------------------------------------------------------'
  if added.count > 0
    added.each do |item|
      # puts  File.join(Rails.root, 'public', 'images', item)
      photo = File.open( item )
      e = Entry.create({:image => photo, :status => 1})
      # e.save
      pp e.valid?
    end
  end
end
listener.start
