Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, APP_CONFIG['tw_app_id'], APP_CONFIG['tw_app_secret']
end
